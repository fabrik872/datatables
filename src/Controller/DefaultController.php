<?php

namespace App\Controller;

use App\Repository\UserDetailsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $columns;

    public function __construct()
    {
        $this->columns = ["id", "username", "first_name", "last_name", "gender", "password", "status"];
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'columns' => $this->columns,
        ]);
    }

    /**
     * @Route("/ajax/data", name="users", methods={"POST"})
     */
    public function users(UserDetailsRepository $userDetailsRepository, Request $request): JsonResponse
    {
        $column = $this->columns[$request->request->get('order')[0]['column']];

        $userDetailsRepository->select(
            $request->request->get('start'),
            $request->request->get('length'),
            $column,
            $request->request->get('order')[0]['dir'],
            $request->request->get('search')['value']
        );

        $json = new \stdClass();
        $json->draw = $request->request->get('draw');
        $json->recordsTotal = $userDetailsRepository->total();
        $json->recordsFiltered = $userDetailsRepository->totalFiltered();
        $json->data = $userDetailsRepository->arrayResults();

        return new JsonResponse($json, 200);
    }
}
