<?php

namespace App\Repository;

use App\Entity\UserDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDetails[]    findAll()
 * @method UserDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDetailsRepository extends ServiceEntityRepository
{
    private $total;
    private $totalFiltered;
    private $arrayResults;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserDetails::class);
    }

    public function select(int $start, int $lenght, string $column, string $type, string $search = null)
    {
        $entityManager = $this->getEntityManager();

        $search = "%" . $search . "%";

        $query = $entityManager->createQuery(
            'SELECT count(u) as totalFiltered
        FROM App\Entity\UserDetails u
        WHERE u.username LIKE :search
        or u.id LIKE :search
        OR u.first_name LIKE :search
        OR u.last_name LIKE :search
        OR u.gender LIKE :search
        OR u.password LIKE :search 
        ')
            ->setParameters([
                "search" => $search,
            ]);
        $this->totalFiltered = $query->getOneOrNullResult()["totalFiltered"];

        $query = $entityManager->createQuery(
            'SELECT count(u) as total
        FROM App\Entity\UserDetails u
        ');
        $this->total = $query->getOneOrNullResult()["total"];

        $query = $entityManager->createQuery(
            'SELECT u
        FROM App\Entity\UserDetails u
        WHERE u.username LIKE :search
        or u.id LIKE :search
        OR u.first_name LIKE :search
        OR u.last_name LIKE :search
        OR u.gender LIKE :search
        OR u.password LIKE :search 
        ORDER BY u.' . $column . ' ' . $type . '
        ')
            ->setMaxResults($lenght)
            ->setFirstResult($start)
            ->setParameters([
                "search" => $search,
            ]);
        $this->arrayResults = $query->getArrayResult();
    }

    public function total(): int
    {
        return $this->total;
    }

    public function totalFiltered(): int
    {
        return $this->totalFiltered;
    }

    public function arrayResults(): array
    {
        return $this->arrayResults;
    }

    // /**
    //  * @return UserDetails[] Returns an array of UserDetails objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserDetails
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
